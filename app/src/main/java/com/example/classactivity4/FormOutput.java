package com.example.classactivity4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Button;

public class FormOutput extends AppCompatActivity {
    TextView val1, val2, val3;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_output);

        button = (Button) findViewById(R.id.button);

        val1 = (TextView) findViewById(R.id.val1id);
        val2 = (TextView) findViewById(R.id.val2id);
        val3 = (TextView) findViewById(R.id.val3id);

        String str1, str2, str3;

        str1 = getIntent().getStringExtra("name");
        str2 = getIntent().getStringExtra("phone");
        str3 = getIntent().getStringExtra("id");

        val1.setText(str1);
        val2.setText(str2);
        val3.setText(str3);
    }
}