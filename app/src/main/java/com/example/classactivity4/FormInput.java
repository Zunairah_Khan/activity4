

package com.example.classactivity4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class FormInput extends AppCompatActivity {

    EditText name, phone, id;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_input);

        name = findViewById(R.id.name);
        phone = findViewById(R.id.phone);
        id = findViewById(R.id.id);
        button = findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str1, str2, str3;
                Intent intent1, intent2, intent3;

                str1 = name.getText().toString();
                str2 = phone.getText().toString();
                str3 = id.getText().toString();

                intent1 = new Intent(getApplicationContext(), FormOutput.class);
                intent2 = new Intent(getApplicationContext(), FormOutput.class);
                intent3 = new Intent(getApplicationContext(), FormOutput.class);

                intent1.putExtra("name", str1);
                intent2.putExtra("phone", str2);
                intent3.putExtra("id", str3);

                startActivity(intent1);
                startActivity(intent2);
                startActivity(intent1);

            }
        });
    }
}